# HTWK Webtech PHP
Projekt für die PHP-Übungen

### Starten und stoppen

Zum Starten müsst ihr nur `docker-compose up` ausführen. Optional könnt ihr `-d` anhängen, damit der Dienst im Hintergrund läuft.
Mit `--build` baut ihr ein neues Images, falls ihr die php-Build-Datei in `build/php.Dockerfile` bearbeitet habt.

Zum Stoppen ganz einfach `docker-compose down` ausführen.

### Datenpersistenz

Die Datenbank-Daten werden standardmäßig in einem Ordner namens `docker-volume/db` abgespeichert. Dieser Ordner wird von Git _NICHT_ getrackt.

### "Edit and View"

Standardmäßig läuft der Webserver unter `http://localhost:8000`.

### Konfigurationsmöglichkeiten

Solltest du den Port / die Adresse ändern wollen, so bearbeite die `.env` (Parameter: `PHP_PORT_HTTP`) und `src/application/config/config.php` (Parameter: `$config['base_url']`) Dateien.

Weitere Änderungen an der `.env`-Datei sollten nicht vorgenommen werden.
