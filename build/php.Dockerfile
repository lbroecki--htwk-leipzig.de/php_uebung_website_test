FROM php:7-apache
ARG PHP_SRC_DIR

RUN a2enmod rewrite
RUN docker-php-ext-install mysqli

RUN mkdir -p /run/php-sess && chown -R www-data:www-data /run/php-sess
RUN chown -R www-data:www-data /run/php-sess
