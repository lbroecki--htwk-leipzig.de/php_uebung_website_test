<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	/**
	 * Controller für unser Login
	 * 
     * die Funktion index() lädt die View "login"
     * 
     * die Funktion validation() prüft die Eingaben der Felder und sendet die Daten an das Model "login_model"
     *   
	 */
public function __construct()
{
    /**
      * CodeIgniter liefert uns eine Controller-Klasse die wir erweitern.
      * 
      * Im Konstruktor prüfen wir die Session_ID, um festzustellen ob der Nutzer angemeldet ist.
      * Dazu können wir die Hilfsklassen session des Frameworks verwenden - der Code sieht wiefolgt aus:
      * if(!$this->session->userdata('id'))
      * Dieser Aufruf liefert FALSE, wenn der Nutzer nicht angemledet ist.
      * 
      */
    parent::__construct();
    if(!$this->session->userdata('id'))
    {
        //AUFGABE: geben Sie die Kontrolle zurück an den Login-Controller (login): redirect('name des ziels');
        redirect('login');
    }
}

function index()
{
    //AUFGABE: laden Sie die View "dashboard" (siehe auch Hinweis in Home.php)
    $this->load->view('dashboard');
}

function logout()
{
    /**
     * Zum Abmelden der Webseite verwenden wir die Hilfsklasse Session - der Code sieht wiefolgt aus:
     */

    //Laden der Session
    $data = $this->session->all_userdata();

    //Löschen der Anmeldung
    foreach($data as $row => $rows_value)
    {
        $this->session->unset_userdata($row);
    }

    /**
     * Nach erfolgreichem Löschen der Session, senden wir den Nutzer zurück zur Startseite
     */

    //AUFGABE: geben Sie die Kontrolle zurück an den Start-Controller (home): redirect('name des ziels');
    redirect('login');
}
}

?>
