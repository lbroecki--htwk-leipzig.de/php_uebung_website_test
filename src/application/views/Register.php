<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<!-- Head/Header -->
<?php
include('util/head.php');
?>

<!-- Navigation -->
<?php
include('util/nav/nav_after_registration.php');
?>

<!-- Content -->
<div id="text-center py-5 mr-auto">
<div class="container">
  <br />
  <h1 align="center">Registriere Dich auf der Internetseite von Kühren!</h1>
  <br />
  <div class="panel panel-default">
   <div class="panel-body">
   <!--AUFGABE Ergänzen Sie Formularfelder für die Nutzerdaten nach dem folgenden Schema -->
   <form method="post" action="<?php echo base_url(); ?>register/validation">
 <!-- Formularfeld Name -->
    <div class="form-group">
       <label>Vorname</label>
       <input type="text" name="user_name" class="form-control" value="<?php echo set_value('user_name'); ?>" />
       <span class="text-danger"><?php echo form_error('user_name'); ?></span>
    </div>
    <!-- Formularfeld Nachname -->
    <div class="form-group">
       <label>Nachname</label>
       <input type="text" name="user_lastname" class="form-control" value="<?php echo set_value('user_lastname'); ?>" />
       <span class="text-danger"><?php echo form_error('user_lastname'); ?></span>
    </div>
	<!-- Formularfeld Email -->
    <div class="form-group">
       <label>Email-Adresse</label>
       <input type="text" name="user_email" class="form-control" value="<?php echo set_value('user_email'); ?>" />
       <span class="text-danger"><?php echo form_error('user_email'); ?></span>
    </div>
    <!-- Formularfeld Passwort -->
    <div class="form-group">
       <label>Passwort</label>
       <input type="text" name="user_password" class="form-control" value="<?php echo set_value('user_password'); ?>" />
       <span class="text-danger"><?php echo form_error('user_password'); ?></span>
    </div>

    <!-- Button Register -->
     <div class="form-group">
      <input type="submit" name="register" value="Register" class="btn btn-info" />
     </div>
    </form>
   </div>
  </div>
 </div>

</div>


<!-- Footer/Script -->
<?php
include('util/footer.php');
?>

 </body>
</html>
