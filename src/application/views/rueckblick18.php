<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!DOCTYPE html>
<html lang="en">

<!--Head/Header->
<?php
include('util/head.php');
?>

<!--Navigation-->
<?php
include('util/nav/nav_throwback.php');
?>

<!-- Hier folgt der Content -->
<div id="mainbody">
    <div id="content">
        <div id="description">
            <div id="throwback" class="row text-center d-flex justify-content-center pt-2">
                            <!-- Grid column -->
                            <div class="col-md-2 mb-3">
                                <h6 class="text-uppercase">
                                    <a href="<?php echo base_url(); ?>rueckblick19">2019</a>
                                </h6>
                            </div>
                            <!-- Grid column -->
                            <div class="col-md-2 mb-3">
                                <h6 class="text-uppercase">
                                    <a href="<?php echo base_url(); ?>rueckblick18">2018</a>
                                </h6>
                            </div>
                            <!-- Grid column -->
                            <div class="col-md-2 mb-3">
                                <h6 class="text-uppercase">
                                    <a href="<?php echo base_url(); ?>rueckblick17">2017</a>
                                </h6>
                            </div>
                                        </div>
            <hr>
            <h1>8. Dezember 2018 – Weihnachtsmarkt am Vereinshaus</h1>
            <p>Mit musikalischer Unterhaltung durch die Singegruppe der Grundschule, die trotzt des Regens alles gegeben
                haben. Vielen Dank nochmal! <br>
                Ein Weihnachtskonzert des Gemischten Chores und eines Schülerchores des Gymnasium Wurzen fand in der
                benachbarten Kirche statt. Der Weihnachtsmann verteilte kleine Überraschungen und in der
                Kinderbastelecke sind wunderschöne Engel aus Korken entstanden. Zum Adventskaffee konnten sich die
                Besucher im gemütlichen Vereinsraum die Stolle schmecken lassen und miteinander ins Gespräch kommen. Wie
                immer war auch das Museum und die Wäschemangelausstellung für Interessierte geöffnet. Die
                Elefantenkalender 2019 – mit Motiven von Kindern aus dem Malwettbewerb – war heiß begehrt.<br>
            </p>
            <hr>
            <h1>18. November 2018 – Einweihung der neuen Gedenktafel auf dem Friedhof Kühren </h1>
            <p>In enger Zusammenarbeit mit Kirchgemeinde konnte eine neue Gedenktafel zum Gedenken an die Opfer des 1.
                Weltkrieges geschaffen werden. Diese wurde am Volkstrauertag von unserer Pfarrerin feierlich eingeweiht.<br>
            </p>
            <p><a href="http://www.heimatverein-kuehren.de/wp-content/uploads/2018/02/Gedenktafel.jpg"
                  data-slb-active="1" data-slb-asset="311876285" data-slb-internal="0" data-slb-group="slb"><img
                    class="size-medium wp-image-1109 alignleft"
                    src="http://www.heimatverein-kuehren.de/wp-content/uploads/2018/02/Gedenktafel-212x300.jpg" alt=""
                    width="212" height="300"
                    srcset="http://www.heimatverein-kuehren.de/wp-content/uploads/2018/02/Gedenktafel-212x300.jpg 212w, http://www.heimatverein-kuehren.de/wp-content/uploads/2018/02/Gedenktafel-768x1086.jpg 768w, http://www.heimatverein-kuehren.de/wp-content/uploads/2018/02/Gedenktafel-724x1024.jpg 724w, http://www.heimatverein-kuehren.de/wp-content/uploads/2018/02/Gedenktafel.jpg 1240w"
                    sizes="(max-width: 212px) 100vw, 212px"></a></p>
            <hr>
            <h1>2. Oktober 2018 – Vereinsball</h1>
            <p>Geselliges Beisammensein aller Vereine von Kühren.<br>
                Organisiert vom Heimatverein Kühren, Motto: 130 Jahre „Elefantenmord zu Kühren“</p>
            <hr>
            <h1>29. September 2018 – Museumstag </h1>
            <p>Die Chronisten luden traditionell zum Museumsnachmittag ein. Die Kaffeetafel wartete mit selbstgebackenem
                Kuchen auf.<br>
            </p>
            <hr>
            <h1>18. August 2018 – Sommerkino am Naturbad </h1>
            <p>Nun schon zum dritten mal fand am Naturbad das Sommerkino statt. Gezeigt wurde diesmal der Film „Wo ist
                Fred?“ mit Til Schweiger<br>
            </p>
            <hr>
            <h1>16. Juni – Lanz-Bulldog-Ausfahrt </h1>
            <p>Die Fahrt ging nach Sermuth. Dort wo sich Zwickauer- und Freiberger Mulde vereinigen. Nach der Rückkehr
                in Kühren, fanden sich alle noch zu einem gemütlichen Grillabend am Naturbad ein.<br>
            </p>
            <hr>
            <h1>2. und 3. Juni 2018 – Dorf- und Kinderfest</h1>
            <p><strong>Lebendig und vielseitig – so ist Kühren</strong></p>
            <p>Ein bunt gemischtes Programm mit den vielfältigsten Angeboten lockte am 02. und 03. Juni 2018 viele
                Kührener, Besucher und Neugierige auf den Festplatz zum traditionellen Dorf- und Kinderfest 2018.</p>
            <p>… und doch war an diesem Wochenende Einiges anders!</p>
            <p>Erstmals begann dieses Fest mit einem Konzert, gestaltet von den Bläsern in der Kührener Kirche.&nbsp;
                Verlesene Texte und dazu passende Musikstücke stimmten auf das bevorstehende Wochenende ein. Im
                Anschluss daran führte ein musikalisch begleiteter Lampionumzug die Kleinen und Großen zum Naturbad, wo
                bereits die nächste Attraktion wartete. Zwei Schlauchboote des DRK Wurzen nahmen die Jüngsten an Bord,
                um ihnen paddelnd einen Vorgeschmack auf den Sonntag zu bieten. Nicht nur die Kinder hatten Spaß – auch
                die vier jungen Mädchen von der Wasserwacht Wurzen erfreuten sich in ihrem Vielsitzer. Im Festzelt
                sorgte unterdessen die Gugge aus Belgern – in ihren farbenfrohen Kostümen und mit ihren originellen
                Instrumenten – für gute Laune. Auch der extra für dieses Dorf- und Kinderfest kreierte Anhänger wurde
                von den Gästen gelobt und war sehr schnell vergriffen.</p>
            <p>Der Sonntag startete bei herrlichstem Sonnenschein mit einem wunderschönen Chorprogramm, gefolgt von den
                Lampertswalder Blasmusikern und einem überzeugenden Programm der Tanzgruppe vom Hort Kühren. Leckere
                Kuchen und verschiedene Eisspezialitäten überraschten die Gäste. Ein Besuch des Zeltes der
                Ortschronisten rundete den Nachmittag ab. Darüber hinaus sorgte die sportliche Einlage im Kisten
                stapeln, das Luftgewehrschießen und der Umgang mit Pfeil und Bogen für Spaß und Spannung bei den
                Teilnehmern. Für die Besten im Malwettbewerb zum Thema „130 Jahre Elefantenmord zu Kühren“ gab es
                attraktive Preise zu gewinnen. Herrlich bemalte Kinder und belebte Stände prägten das Geschehen auf dem
                Festgelände. Der Höhepunkt des Tages war am Nachmittag das Badewannenrennen. Weniger Boote als im
                Vorjahr gingen an den Start und kämpften um den Wanderpokal. Es war ein spannendes und zugleich lustiges
                Wettrennen, aus welchem der Dreisitzer „Auf der Tim Wiese 1“ als Sieger hervorging.</p>
            <p>Am Ende des Rennens fand der Moderator noch eine passende Überleitung zu dem nächsten Dorf- und
                Kinderfest. Lautstark und unüberhörbar verkündete er, dass 2019 jeder Verein in Kühren mit einem eigenen
                Boot an den Start gehen wird.</p>
            <p>Na, dann lassen wir uns mal überraschen…</p>
            <p>Ein herzliches Dankeschön ergeht unsererseits an die vielen freiwilligen Helfer für ihre Zeit, ihre Kraft
                und manch‘ spontanen Einsatz.&nbsp;</p>
            <p>An dieser Stelle möchten wir uns auch bei allen Sponsoren bedanken, die das Dorf- und Kinderfest 2018 mit
                finanziellen Zuwendungen und Sachspenden unterstützten und es zu einem gelungenen Wochenende werden
                ließen.</p>
            <p></p>
            <hr>
            <h1>30. April 2018 – 18 Uhr Maibaumstellen </h1>
            <p>Die Jagdhornbläser sorgten für die musikalische Umrahmung und die Kinder der Kita Rüsselchen erfreuten
                die Gäste mit Ihrem Programm. Das Museum sowie die Wäschemangelausstellung luden zum Schauen und Staunen
                ein. In der Kinderbastelecke wurden diesmal bunte Windspiele geschaffen.<br>
            </p>
            <hr>
            <h1>14. April 2018 – Frühjahrsputz am Bad</h1>
            <p> Entschlammung des Badeteichs, Säuberung der sanitären Anlagen, Pflege der Grasflächen, Reinigung der
                Bühne und der Wege am Bad – das Stand auf dem Programm.<br>
                Parallel dazu brachte der Chor die Räume des Vereinshauses auf Hochglanz.<br>
                Anschließend stand für alle ein Imbiss bereit.<br>
            </p>
            <hr>
            <h1>20. März 2018 – Vortrag – Patientenverfügung/Vorsorgevollmacht </h1>
            <p>Wissenswertes, Interessantes und Aufschlussreiches war von Rechtsanwältin Frau Schlittgen zu diesem
                wichtigen Thema zu erfahren.<br>
            </p>
            <p>&nbsp;</p>
        </div>
    </div>
</div>

<!-- Footer -->
<?php
include('util/footer.php');
?>

</body>
</html>
