<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<!-- Head/Header -->
<?php
include('util/head.php');
?>

<!-- Navigation -->
<?php
include('util/nav/nav_termine.php');
?>

<!-- Content -->
<div id="mainbody">
    <div id="content">
        <div id="description">
            <h1>Veranstaltungen</h1>
            <h3>Hier werden alle lokalen Veranstaltungen angezeigt!</h3>
        </div>

        <div class="container-fluid">
            <div class="row spacing justify-content-around">
                <div class="card col-3">
                    <img src="images/pic02.jpg"
                         class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">November</h5>
                        <p class="card-text">Vorschau für Termine im Januar ... </p>
                        <a href="#" class="btn btn-primary">Was bringt der November?</a>
                    </div>
                </div>
                <div class="card col-3">
                    <img src="images/pic06.jpg"
                         class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Dezember</h5>
                        <p class="card-text">Vorschau für Termine im Januar ... </p>
                        <a href="#" class="btn btn-primary">Was bringt der Dezember?</a>
                    </div>
                </div>
                <div class="card col-3">
                    <img src="images/pic07.jpg"
                         class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Januar</h5>
                        <p class="card-text">Vorschau für Termine im Januar ... </p>
                        <a href="#" class="btn btn-primary">Was bringt der Januar?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>

<!-- Footer -->
<?php
include('util/footer.php');
?>

</body>
</html>
