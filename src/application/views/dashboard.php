<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!doctype html>
<html lang="en">

<!--Head/Header->
<?php
include('util/head_after_registration.php');
?>

<!--Navigation-->
<?php
include('util/nav/nav_after_registration.php');
?>

<!--Main Layout-->
<main class="text-center py-5">

  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <p align="justify">Willkommen! Du bist jetzt angemeldet.</p>

      </div>
    </div>
  </div>

</main>
<!-- Footer/Script -->
<?php
include('util/footer.php');
?>

 </body>
</html>
