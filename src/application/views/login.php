<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!DOCTYPE html>
<html lang="en">

<!--Head/Header->
<?php
include('util/head.php');
?>

<!--Navigation-->
<?php
include('util/nav/nav_login.php');
?>

<!--Main Layout-->
<main class="text-center py-5">
	<div class="container">
		<h1>Melde Dich auf der Internetseite Kühren an!</h1>
		<div class="panel panel-default">
			<div class="panel-body">
				<?php
                if($this->session->flashdata('message'))
				{
				echo '
				<div class="alert alert-success">
					'.$this->session->flashdata("message").'
				</div>
				';
				}
				?>

				<form method="post" action="<?php echo base_url(); ?>login/validation">
					<!-- Formularfeld Email -->
					<div class="form-group">
						<label>Email-Adresse</label>
						<input type="text" name="user_email" class="form-control" value="<?php echo set_value('user_email'); ?>" />
						<span class="text-danger"><?php echo form_error('user_email'); ?></span>
					</div>
					<!-- Formularfeld Passwort -->
					<div class="form-group">
						<label>Passwort</label>
						<input type="text" name="user_password" class="form-control" value="<?php echo set_value('user_password'); ?>" />
						<span class="text-danger"><?php echo form_error('user_password'); ?></span>
					</div>
					<!-- 2 Buttons für Login und Register (Register ruft Controller auf) -->
					<div class="form-group">
						<input type="submit" name="login" value="Login" class="btn btn-info" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>register">Register</a>
					</div>
				</form>
			</div>
		</div>
	</div>

</main>
<!-- Footer -->
<?php
include('util/footer.php');
?>

</body>
</html>
