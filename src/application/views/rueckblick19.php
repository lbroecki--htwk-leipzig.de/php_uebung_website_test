<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!DOCTYPE html>
<html lang="en">

<!--Head/Header->
<?php
include('util/head.php');
?>

<!--Navigation-->
<?php
include('util/nav/nav_throwback.php');
?>

<!-- Hier folgt der Content -->
<div id="mainbody">
    <div id="content">
        <div id="description">
            <div id="throwback" class="row text-center d-flex justify-content-center pt-2">
                <!-- Grid column -->
                <div class="col-md-2 mb-3">
                    <h6 class="text-uppercase">
                        <a href="<?php echo base_url(); ?>rueckblick19">2019</a>
                    </h6>
                </div>
                <!-- Grid column -->
                <div class="col-md-2 mb-3">
                    <h6 class="text-uppercase">
                        <a href="<?php echo base_url(); ?>rueckblick18">2018</a>
                    </h6>
                </div>
                <!-- Grid column -->
                <div class="col-md-2 mb-3">
                    <h6 class="text-uppercase">
                        <a href="<?php echo base_url(); ?>rueckblick17">2017</a>
                    </h6>
                </div>
                            </div>
            <hr>
            <h1>6. Dezember 2019 – Weihnachtsmarkt</h1>
            <p>Der Kührener Weihnachtsmarkt 2019 fand am Freitag, den 6. Dezember ab 15 Uhr auf dem Gelände des
                Kindergartens und der Schule statt.&nbsp; Erstmals wurde die Veranstaltung in Zusammenarbeit des
                Förderverein Kita Kühren e. V. organisiert.</p>
            <p>Für jeden war etwas dabei. Um 15.30 Uhr starteten die Kinder mit ihrem Weihnachtsprogramm in der
                Turnhalle. Danach konnten die Besucher sich an einem Feuerspektakel der besonderen Art erfreuen. Die
                Kindereisenbahn war gut besucht. Es gab eine Tombola, Weihnachtsbastelstube und verschiedene
                Verkaufsstände. Natürlich durfte der Weihnachtsmann nicht fehlen, der kleine Geschenke für die Kinder
                dabei hatte. Für weihnachtliche Melodien sorgte der Posaunenchor Kühren-Sachsendorf. Dank der
                Freiwilligen Feuerwehr Kühren konnten die Besucher gemütlich im beheizten Festzelt sitzen und den
                selbstgebackenen Kuchen, Glühwein, Roster und andere Leckereien genießen. Ein zweites Zelt bot der
                Tombola und den Verkaufsständen ein Dach über dem Kopf.<br>
            </p>
            <p>Wir bedanken uns beim Förderverein Kita e. V. und der Freiwilligen Feuerwehr Kühren für die gute
                Zusammenarbeit und freuen uns schon auf weitere gemeinsame Projekte.</p>
            <p></p>
            <p></p>
            <hr>
            <h1>5. November 2019 – Mitgliederversammlung mit Vorstandswahlen</h1>
            <p>Wir möchten Ihnen das Ergebnis unserer Vorstandswahlen mitteilen. </p>
            <p><strong>Ab sofort setzt sich der Vorstand wie folgt zusammen:</strong></p>
            <p>Ronny Anders<br>
                Steffen Dieke<br>
                Janine Fink<br>
                Carmen Haberland<br>
                Monika Schliemann<br>
            </p>
            <p><strong>Im erweiterten Vorstand arbeiten mit:</strong></p>
            <p>Ronny Fink<br>
                Alexander Sachse<br>
                Sebastian Düring</p>
            <p><strong>Die Revisionskommission bilden:</strong></p>
            <p>Regina Jähnigen<br>
                Christina Loskarn</p>
            <p>Der Vorstand arbeitet gleichberechtigt. Die Ämter werden nicht einer bestimmten Person zugeordnet.
                Lediglich die Kassenverwaltung bleibt in den Händen von Janine Fink. Alle gewählten Vorstandsmitglieder
                sind gleichermaßen Ansprechpartner für die Belange des Vereins.</p>
            <hr>
            <h1>28. September 2019 – Museumsnachmittag</h1>
            <p>Wie immer gut vorbereitet von unseren Chronisten, die eine Dia-Show mit Blick auf die zurückliegenden
                Highlights in Kühren zusammengestellt hatten. Da die Kaffeetafel im Vereinshaus nicht möglich war, fand
                die Veranstaltung auf dem Hof Haberland statt, wo sich auch unsere Wäschemangelausstellung
                befindet. </p>
            <hr>
            <h1>14. September 2019 – Vereinsfahrt</h1>
            <p>Mit dem Bus ging es nach Wermsdorf. Dort hatten wir eine Führung durch das Schloss Hubertusburg, wo
                gerade eine Sonderausstellung zu Gast war. Es ging um die sogenannte „Traumhochzeit des Jahrhunderts“
                von Friedrich August und Maria Josepha. Die Führung war anschaulich und interessant, wir konnten auch
                Räume im hinteren Teil des Schlosses besichtigen, welche sonst nicht zugänglich sind. Nach der Führung
                haben wir im Schlosscafe Praetsch Kaffee getrunken und nach einem Spaziergang durch die Schlossanlage
                ging die Fahrt zurück nach Kühren.</p>
            <hr>
            <h1>24. August- Sommerkino</h1>
            <p>Gezeigt wurde der Film „Der Junge muss an die frische Luft“. Das Wetter war perfekt und der Film kam gut
                an, deshalb konnten wir uns über zahlreiche Gäste freuen.</p>
            <hr>
            <h1>5. Juli 2019 – Vereinsgrillen</h1>
            <p>Zu einem gemütlichen Grillabend trafen sich die Vereinsmitglieder. Bei leckerem Essen und guten
                Gesprächen verging die Zeit wie im Flug. Unser Naturbad bot eine wunderschöne Kulisse für den Abend.</p>
            <p>Ein Dankeschön an alle, die zur kulinarischen Vielfalt des Abends beigetragen haben.</p>
            <hr>
            <h1>8. Juni 2019 – Lanz-Bulldog-Ausfahrt</h1>
            <p>Wiedereinmal war die Mulde unser Ziel. Bei wunderbarem Sonnenschein, wenn auch etwas stürmisch, ging die
                Fahrt nach Gruna zum Fähranleger, ein schönes Fleckchen Erde. Nach einer Rast mit Kaffee und Kuchen
                kehrte wir zurück nach Kühren,&nbsp; wo sich alle noch zu einem gemütlichen Grillabend am Naturbad
                trafen.</p>
            <hr>
            <h1>1. und 2. Juni 2019 – Dorf- und Kinderfest</h1>
            <p>Ein herzliches Dankeschön ergeht unsererseits an die vielen freiwilligen Helfer für ihre Zeit, ihre Kraft
                und manch‘ spontanen Einsatz.</p>
            <p>An dieser Stelle möchten wir uns auch bei allen <a href="http://www.heimatverein-kuehren.de/sponsoren/">Sponsoren</a>
                bedanken, die das Dorf- und Kinderfest 2019 mit finanziellen Zuwendungen und Sachspenden unterstützten
                und es zu einem gelungenen Wochenende werden ließen.</p>
            <hr>
            <h1>Mai 2019</h1>
            <p>Eine neue Sitzgarnitur lädt ab sofort zum Verweilen am Naturbad in Kühren ein.</p>
            <p>Möglich wurde das Projekt durch Gelder aus dem PS-Lotteriesparen der Sparkasse Muldental. Daraus werden
                gemeinnützige Projekte in der Region unterstützt. <br>
                Vielen Dank dafür!</p>
            <p><a href="http://www.zimmereiweber.de/">Zimmerei Weber</a> aus Luppa fertigte die Sitzgarnitur an, welche
                noch durch den Heimatverein in Eigenleistung gestrichen wurde und sich nun großer Beliebtheit erfreut.
            </p>
            <p><a href="http://www.heimatverein-kuehren.de/wp-content/uploads/2019/07/Sitzgarnitur-kl.jpg"
                  data-slb-active="1" data-slb-asset="1407277194" data-slb-internal="0" data-slb-group="slb"><img
                    class="alignleft size-medium wp-image-1280"
                    src="http://www.heimatverein-kuehren.de/wp-content/uploads/2019/07/Sitzgarnitur-kl-300x225.jpg"
                    alt="" width="300" height="225"
                    srcset="http://www.heimatverein-kuehren.de/wp-content/uploads/2019/07/Sitzgarnitur-kl-300x225.jpg 300w, http://www.heimatverein-kuehren.de/wp-content/uploads/2019/07/Sitzgarnitur-kl-768x576.jpg 768w, http://www.heimatverein-kuehren.de/wp-content/uploads/2019/07/Sitzgarnitur-kl.jpg 980w"
                    sizes="(max-width: 300px) 100vw, 300px"></a></p>
            <hr>
            <h1>30. April 2019 – 18 Uhr Maibaumstellen</h1>
            <p>Der Posaunenchor Kühren und die Kinder der Musikschule Fröhlich sorgten für die musikalische Umrahmung.
                Das Museum sowie die Wäschemangelausstellung luden zum Schauen und Staunen ein. In der Kinderbastelecke
                wurden diesmal bunte Papierblumen gezaubert.</p>
            <hr>
            <h1>6. April 2019 – Frühjahrsputz am Bad</h1>
            <p>Entschlammung des Badeteichs, Säuberung der sanitären Anlagen, Pflege der Grasflächen, Reinigung der
                Bühne und der Wege am Bad – das Stand auf dem Programm. Außerdem wurde der Stellplatz für unsere neue
                Sitzgarnitur vorbereitet.<br>
                Parallel dazu brachte der Chor die Räume des Vereinshauses auf Hochglanz.<br>
                Anschließend stand für alle ein Imbiss bereit.</p>
        </div>
    </div>
</div>

<!-- Footer -->
<?php
include('util/footer.php');
?>

</body>
</html>
