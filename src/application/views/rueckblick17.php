<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>


<!DOCTYPE html>
<html lang="en">

<!--Head/Header->
<?php
include('util/head.php');
?>

<!--Navigation-->
<?php
include('util/nav/nav_throwback.php');
?>

<!-- Hier folgt der Content -->
<div id="mainbody">
    <div id="content">
        <div id="description">
           <div id="throwback" class="row text-center d-flex justify-content-center pt-2">
                           <!-- Grid column -->
                           <div class="col-md-2 mb-3">
                               <h6 class="text-uppercase">
                                   <a href="<?php echo base_url(); ?>rueckblick19">2019</a>
                               </h6>
                           </div>
                           <!-- Grid column -->
                           <div class="col-md-2 mb-3">
                               <h6 class="text-uppercase">
                                   <a href="<?php echo base_url(); ?>rueckblick18">2018</a>
                               </h6>
                           </div>
                           <!-- Grid column -->
                           <div class="col-md-2 mb-3">
                               <h6 class="text-uppercase">
                                   <a href="<?php echo base_url(); ?>rueckblick17">2017</a>
                               </h6>
                           </div>
                                       </div>
            <hr>
            <h1>09. Dezember 2017 – Weihnachtsmarkt</h1>
            <p><img class="size-medium wp-image-667 alignright"
                    src="http://www.heimatverein-kuehren.de/wp-content/uploads/2018/01/2017-Weihnachtsbaum-225x300.jpg"
                    alt="" width="200" height="270"
                    srcset="http://www.heimatverein-kuehren.de/wp-content/uploads/2018/01/2017-Weihnachtsbaum-225x300.jpg 225w, http://www.heimatverein-kuehren.de/wp-content/uploads/2018/01/2017-Weihnachtsbaum-768x1024.jpg 768w, http://www.heimatverein-kuehren.de/wp-content/uploads/2018/01/2017-Weihnachtsbaum.jpg 1000w"
                    sizes="(max-width: 225px) 100vw, 225px"></p>
            <p>Gemütliches Miteinander – so kann man es wohl nennen.<br>
                Der riesige geschmückte Weihnachtsbaum vorm Vereinshaus kündigte schon im Voraus den diesjährigen
                Weihnachtsmarkt an. Extra für diesen Tag wurden Buden herangeholt und die Freiwilligen Feuerwehr Kühren
                stellte auch diesmal ihr Festzelt zur Verfügung, so dass für Gemütlichkeit gesorgt war. Leckeres vom
                Grill oder Gulaschsuppe, dazu ein Glühwein, so ließen es sich die Besucher gut gehen. Auch die
                selbstgebackenen Waffeln und Schokoäpfel fanden ihre Abnehmer.</p>
            <p>Im Vereinshaus wurde die Kaffeetafel gedeckt. Traditionell ließen sich die Besucher die Stolle schmecken,
                um anschließend noch einen Rundgang im Museum zu machen. Auch die neue Wäschemangelausstellung, auf dem
                Hof Haberland, war geöffnet und wurde gut besucht. Begeistert bastelten die Kinder Weihnachtssterne und
                konnten gar nicht genug davon bekommen.</p>
            <p>Der Höhepunkt, auf den sich besonders die Kinder freuten, war diesmal das Puppentheater. Die Gastwirtin
                Kerstin Reinicke stellte den benachbarten Saal zur Verfügung. Dort zeigte „Rosi Lampe“ aus Leipzig das
                beliebte Märchen „Rotkäppchen und der Wolf“. Im Anschluss holte der Weihnachtsmann die Kinder dort
                wieder ab um sie – zurück auf dem Weihnachtsmarkt – mit ein paar süßen Überraschungen zu erfreuen.</p>
            <p></p>
            <hr>
            <h1>07 November 2017 – Jahreshauptversammlung</h1>
            <hr>
            <h1>21 Oktober 2017 – Wäschemangelaustellung</h1>
            <ul>
                <li>8 funktionstüchtige Rollen anno „dazumal“</li>
                <li>Unterhaltung mit der Gruppe „Vergissmeinnicht“</li>
                <li>Kaffee &amp; frischer Kartoffelkuchen</li>
                <li>Bastelspaß für die „Kleinen“ – Kartoffeldruck</li>
            </ul>
            <p>
                <a href="http://www.lvz.de/Region/Wurzen/Museumstag-Heimatverein-Kuehren-geht-mit-Einwohnern-auf-die-Rolle"
                   target="_blank" rel="noopener noreferrer">Pressestimme LVZ 24.10.2017</a> / <a
                    href="http://www.lvz.de/Region/Wurzen/Kuehren-geht-mit-neuer-Ausstellung-auf-die-Rolle"
                    target="_blank" rel="noopener noreferrer">19.10.2017</a></p>
            <p></p>
            <hr>
            <h1>26. August 2017 – Vereinsfahrt</h1>
            <p>Die Fahrt führte nach Grimma. Bei einer kulinarischen Stadtführung mit Frank Ziegra konnten wir auf
                schmackhafte Art und Weise Grimma ganz neu entdecken. Der Gästeführer zeigte uns, wie und wo früher
                gekocht wurde und fütterte uns unterwegs mit vielen lustigen Anekdoten zur Stadt und deren Geschichte.
                Anschließend gings mit dem Boot auf der Mulde nach Höfgen und von dort aus mit dem Bus wieder zurück
                nach Kühren, wo wir den Abend bei unserer Gastwirtin im „Elefanten“ ausklingen ließen.</p>
            <p></p>
            <hr>
            <h1>12. August 2017 – Sommer-Kino</h1>
            <p></p>
            <hr>
            <h1>Samstag, 17. Juni 2017 – Lanz-Bulldog-Ausfahrt</h1>
            <p>Diesmal führte unsere Ausfahrt nach Belgern. Das Wetter war ideal und so bleibt der Tag für alle
                Beteiligten in guter Erinnerung.</p>
            <p></p>
            <hr>
            <h1>10. und 11. Juni 2017 – Dorf- und Kinderfest</h1>
            <p>Kühren startet in die Sommersaison!</p>
            <p>Mit einem vielfältigen Angebot begeisterte der Heimatverein Kühren e. V. „Alt“ und „Jung“ am vergangenen
                Wochenende.</p>
            <p>Ein herrlicher Sonntag lockte viele Kührener und Gäste an das Freibad des Ortes zum traditionellen Dorf-
                und Kinderfest 2017. Mit einem erfrischenden Chorprogramm und einer Weltreise von Schülern in
                farbenfrohen und landestypischen Kostümen begann der Nachmittag. Viele leckere Kuchen, toller Eiskaffee
                und weitere Spezialitäten überraschten die Besucher. Sportliche Einlagen – wie Stiefelweitwurf und
                Schießen – ließen alle mit Spaß und Freude bei der Sache sein, denn die Preise waren attraktiv. Der
                Höhepunkt war – wie jedes Jahr – das Elefantenwannenrennen. Ein spannendes Geschehen bot sich allen
                bereits in der ersten Runde, ein Zweisitzer im harten Konkurrenzkampf mit einem Kindervierer. Der Sieg
                und der Pokal gingen zwar an den Zweisitzer, aber zur Siegerehrung wurde der Pokal an die Kinder
                weitergereicht. Eine sehr rührende Geste, die dem Wert des Elefantenwannenrennens einen enormen
                Aufschwung verlieh und uns im nächsten Jahr sicher noch mehr Kinderbeteiligung garantiert.</p>
            <p>An dieser Stelle ein ganz herzliches Dankeschön an die vielen freiwilligen Helfer und Sponsoren, die das
                Dorf-und Kinderfest unterstützten und es zu einem gelungenen Tag werden ließen!</p>
            <p></p>
            <hr>
            <h1>30. April 2017 – 6. Maibaumstellen am Vereinshaus</h1>
            <p>Auch in diesem Jahr setzten wir unseren Maibaum feierlich. Wie im vergangenen Jahr schon, ziert ihn ein
                zweiter Kranz, gestaltet vom Förderverein Kita Kühren.</p>
            <p>Musikalisch führten die Jagdhornbläser durch den Abend. Die Kinder freuten sich über die Bastelecke,
                diesmal wurden Windräder gebastelt, welche auch gleich gut getestet werden konnten, denn der Wind blies
                ordentlich.</p>
            <p></p>
            <hr>
            <h1>Frühjahrsputz am 22. April 2017</h1>
            <p>Diesmal teilten sich die Freiwilligen auf. Die Männer kümmerten sich in gewohnter Manier um unser
                Naturbad. Die Frauen brachten das Vereinshaus samt Museum auf Hochglanz.</p>
            <p>Nach getaner Arbeit gabs Würstchen und Kartoffelsalat.</p>
            <hr>
            <h1>Verkehrsteilnehmerschulung am 14. März 2017</h1>
        </div>
    </div>
</div>

<!-- Footer -->
<?php
include('util/footer.php');
?>

</body>
</html>
