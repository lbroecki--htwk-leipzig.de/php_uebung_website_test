<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<!-- Head/Header -->
<?php
include('util/head.php');
?>

<!-- Navigation -->
<?php
include('util/nav/nav_home.php');
?>

<!-- Content -->
<div id="text-center py-5 mr-auto">
    <div id="content">
            <div id="description">
                <h1>Herzlich Willkommen auf unser Internetseite!</h1>
                <p>Kühren ist ein reguliertes Gassendorf mit älterem Dorfkern. <br>
                    Geprägt ist das Dorf durch die typischen Dreiseitenhöfe.<br><br>
                    Die Entwicklung des Ortes war stark von den Verkehrswegen der Eisenbahn und der Fernverkehrsstraße B6 beeinflusst. <br>
                    Kühren zeichnete sich durch seine Landwirtschaft aus, die bis heute noch, vertreten durch mehrere Unternehmen, vorhanden ist. <br>
                    Mit seiner starken Ansiedlung und der hohen Einwohnerzahl ist Kühren der größte aller Ortsteile von Wurzen.<br><br>
                    Beim Stöbern auf unserer Internetseite können Sie weitere Details und Informationen über unsere Arbeit und
                    über unsere Heimat erfahren.<br>
        </div>
    </div>
        <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="row justify-content-between">
                <div class="col-4 preview-card">
                    <a href="<?php echo base_url(); ?>termine">
                        <div class="card">
                            <img src="images/pic01.jpg" height="120" class="card-img-top" alt="...">
                            <div class="card-body prewview-card-body">
                                <h5 class="card-title preview-card-title">Termine</h5>
                                <p class="card-text">Was bringt die Zukunft? <br>Hier geht es zu Terminen und Veranstaltungen in Kühren.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-4 preview-card">
                    <a href="<?php echo base_url(); ?>rueckblick19">
                        <div class="card">
                            <img src="images/pic02.jpg" height="120" class="card-img-top" alt="...">
                            <div class="card-body prewview-card-body">
                                <h5 class="card-title preview-card-title">Rückblick</h5>
                                <p class="card-text">Was ist in den letzten Jahren passiert? Hier geht es zu den Rückblicken 2016-2019.</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-4 preview-card">
                    <a href="./Chronik/ortcharakteristik.html">
                        <div class="card">
                            <img src="/images/pic03.jpg" height="120" class="card-img-top" alt="...">
                            <div class="card-body prewview-card-body">
                                <h5 class="card-title preview-card-title">Chronik</h5>
                                <p class="card-text">Wieso? Weshalb? Warum? Hier geht es zur Ortschronik und Ortscharakteristik.</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
    </div>
</div>
</div>
<br>

<!-- Footer/Script -->
<?php
include('util/footer.php');
?>


</body>
</html>
