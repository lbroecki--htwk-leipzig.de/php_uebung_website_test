<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<!--Head/Header->
<?php
include('util/head.php');
?>

<!--Navigation-->
<?php
include('util/nav/nav_contact.php');
?>

<!--Main Layout-->
<main class="text-center py-5">
	<div class="container">
		<h1>Kontaktformular</h1>
		<div class="panel panel-default">
			<div class="panel-body">
				<?php
                if($this->session->flashdata('message'))
				{
				echo '
				<div class="alert alert-success">
					'.$this->session->flashdata("message").'
				</div>
				';
				}
				?>

				<form method="post" action="<?php echo base_url(); ?>login/validation">
                        <div id="content">
                            <div id="description">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form_name">Vorname *</label>
                                            <input id="form_name" type="text" name="name" class="form-control"
                                                   placeholder="Bitte hier Vornamen eingeben" required="required"
                                                   data-error="Firstname is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form_lastname">Nachname *</label>
                                            <input id="form_lastname" type="text" name="surname" class="form-control"
                                                   placeholder="Bitte hier Nachnamen eingeben" required="required"
                                                   data-error="Lastname is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form_email">Email *</label>
                                            <input id="form_email" type="email" name="email" class="form-control"
                                                   placeholder="Bitte hier Email eingeben" required="required"
                                                   data-error="Valid email is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="form_telefon">Telefon (optional)</label>
                                            <input id="form_telefon" type="tel" name="telefon" class="form-control"
                                                   placeholder="Bitte hier Telefonnummer eingeben" required="required"
                                                   date-error="Valid telefon is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="form_message">Nachricht *</label>
                                            <textarea id="form_message" name="Nachricht" class="form-control"
                                                      placeholder="Ich möchte folgendes sagen" rows="4" required="required"
                                                      data-error="Please, leave us a message."></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <input type="checkbox" name="datenschutz" value="check">

                                            Ich habe die Informationen zum Datenschutz gelesen. *
                                        </div>

                                        <div class="col-md-12">
                                            <input type="submit" class="btn btn-success btn-send" value="Senden">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-muted">
                                            <strong>*</strong> = Pflichtfelder
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
			</div>
		</div>
	</div>

</main>
<!-- Footer -->
<?php
include('util/footer.php');
?>

</body>
</html>
