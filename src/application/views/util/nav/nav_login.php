<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"></a>
    <!-- Hamburger-Menü-Button bei Responsive -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>">Startseite</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>termine">Termine</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuRückblick" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Rückblick
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>rueckblick19">2019</a>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>rueckblick18">2018</a>
                                    <a class="dropdown-item" href="<?php echo base_url(); ?>rueckblick17">2017</a>
                                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuProjekte" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Projekte
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="./Projekte/elefant.html">Elefant</a>
                    <a class="dropdown-item" href="./Projekte/museum.html">Museum</a>
                    <a class="dropdown-item" href="./Projekte/wäschemangel.html">Wäschemangel</a>
                    <a class="dropdown-item" href="./Projekte/naturbad.html">Naturbad</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuChronik" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Chronik
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="./Chronik/ortcharakteristik.html">Ortcharakteristik</a>
                    <a class="dropdown-item" href="./Chronik/ortchronik.html">Ortchronik</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url(); ?>kontakt">Kontakt<span class="sr-only">(current)</span></a>
            </li>
           <?php if(!$this->session->userdata("id")):?>
                               <ul class="navbar-nav ml-auto">
                               <li class="nav-item">
                                   <a class="nav-link" href="<?php echo base_url();?>login">Login</a>
                               </li>
                               </ul>
                               <?php else:?>
                               <ul class="navbar-nav ml-auto">
                                   <li class="nav-item">
                                       <a class="nav-link" href="<?php echo base_url();?>home/logout">Logout</a>
                                   </li>
                               </ul>
                               <span class="navbar-text">
                               <?=$this->session->userdata('name');?>
                               </span>
                               <?php endif;?>
        </ul>
    </div>
</nav>
