<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Grid row-->
<footer>
    <div id="footer-menu" class="row text-center d-flex justify-content-center pt-4">

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase">
                <a href="#!">Impressum</a>
            </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase">
                <a href="#!">Datenschutz</a>
            </h6>
        </div>
        <!-- Grid column -->

        <!-- Grid column -->
        <div class="col-md-2 mb-3">
            <h6 class="text-uppercase">
                <a href="<?php echo base_url(); ?>kontakt">Kontakt</a>
            </h6>
        </div>
        <!-- Grid column -->
    </div>

    <div id="footer-copyright" class="footer-copyright text-center py-3">© 2020 Copyright:
        <a href="https://www.htwk-leipzig.de"> webtech-team3</a>
    </div>
</footer>


<!-- Script -->
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
