<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/css.css">
    <title>Kühren - Startseite</title>
</head>
<body>
<!-- Header -->
<header id="header">
    <table>
        <th>
        <th><a href="<?php echo base_url(); ?>"><img src="images/Kühren_Logo.png" width="170" height="210"></a></th>
        <th><a href="<?php echo base_url(); ?>">Kühren in Sachsen</a></th>
        </tr>
    </table>
</header>
